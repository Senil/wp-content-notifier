<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin
 * @author     Your Name <email@example.com>
 */
class Tortuga_Content_Notifier_Admin
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $plugin_name The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $version The current version of this plugin.
     */
    private $version;
    /**
     * @var Rest_Client
     */
    private $restClient;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string $plugin_name The name of this plugin.
     * @param      string $version The version of this plugin.
     */
    public function __construct($plugin_name, $version)
    {

        $this->plugin_name = $plugin_name;
        $this->version = $version;
        // init RestClient
        $url = Tortuga_Content_Notifier_Admin_Options::get_endpoint('ca_consumer');
        $this->restClient = new Rest_Client($url, []);
    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Plugin_Name_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Plugin_Name_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/plugin-name-admin.css', array(), $this->version, 'all');

    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Plugin_Name_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Plugin_Name_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/plugin-name-admin.js', array('jquery'), $this->version, false);

    }

    // Init plugin options to white list our options
    public function tcn_admin_options_init()
    {
        register_setting('tcn_admin_options', 'tcn_admin_options', array($this, 'tcn_admin_options_validate'));
    }

    // Add menu page
    public function tcn_admin_add_options_page()
    {
        add_options_page('Tortuga Content Notification Options', 'Tortuga Content Notification', 'manage_options', 'tcn_options', array($this, 'tcn_admin_options_view'));
    }

    // Draw the menu page itself
    public function tcn_admin_options_view()
    {
        include_once 'partials/tcn-admin-options.phtml';
    }
    // Init plugin options to white list our options
    public function tcn_admin_synchronize_menu_init()
    {
        register_setting('tcn_admin_synchronize', 'tcn_admin_synchronize', array($this, 'tcn_admin_synchronize_validate'));
    }
    /**
     * add to management synchronize_menu
     */
    public function tcn_admin_add_synchronize_menu_page()
    {
        add_management_page('Tortuga Synchronization', 'Tortuga Synchronize', 'manage_options', 'tcn_synchronize_menu', array($this, 'tcn_synchronize_menu_view'));
    }

    /**
     * @throws HttpException
     */
    public function tcn_synchronize_menu_view()
    {
        include_once 'partials/tcn-synchronize-menu.phtml';
        $options = $this->getOptions('tcn_admin_synchronize');
        if(!isset($options['synchronize'])) {
            return;
        }
        if ($options['synchronize'] === 'post') {
            $this->syncronizePosts();
        }
        if ($options['synchronize'] === 'term') {
            $this->synchronizeTerms();
        }
    }

    private function syncronizePosts() {
        $posts = get_posts();
        foreach($posts as $index => $post) {
            $id = $post->ID;
            $statusAndMeta = $this->getPostStatusAndMetaData($id);
            $terms = $this->getTermsByPostId($id);
            $posts[$index] = array_merge(['id' => $id, 'post' => $post], $statusAndMeta, $terms);
        }

        $message = [
            'properties' => new ArrayObject(),
            'routing_key' => 'routing_posts',
            "payload" => $posts,
            'payload_encoding' => 'json'
        ];

        $this->restClient->addHeaders(['accept' => 'application/vnd.ca-consumer.v1+json']);
        $this->restClient->post($message);

    }

    /**
     * @throws HttpException
     */
    private function synchronizeTerms() {
        $terms = get_terms(array(
            'taxonomy' => 'category',
            'hide_empty' => false,
        ));

        foreach($terms as $index => $term) {
            $terms[$index] = [
                'id' => $term->term_id,
                'taxonomy' => $term
            ];
        }

        $message = $this->createMessage(
            new ArrayObject(),
            'routing_taxonomies',
            $terms
        );

        $this->restClient->addHeaders(['accept' => 'application/vnd.ca-consumer.v1+json']);
        $this->restClient->post($message);
    }

    /**
     * @param $identifier
     * @return mixed|void
     */
    public function getOptions($identifier)
    {
        return get_option($identifier);
    }

    // Sanitize and validate input. Accepts an array, return a sanitized array.
    public function tcn_admin_options_validate($input)
    {
        // Say our second option must be safe text with no HTML tags
        $input['sometext'] = wp_filter_nohtml_kses($input['sometext']);

        return $input;
    }

    // Sanitize and validate input. Accepts an array, return a sanitized array.
    public function tcn_admin_synchronize_validate($input)
    {
        // Say our second option must be safe text with no HTML tags
        $input['sometext'] = wp_filter_nohtml_kses($input['sometext']);

        return $input;
    }

    /**
     * @param $post_id
     * @param $post
     * @param $isUpdated
     * @throws HttpException
     */
    public function add_post_notification($post_id, $post, $isUpdated)
    {
        // If this is just a revision, don't send
        if (wp_is_post_revision($post_id)) {
            return;
        }

        $statusAndMeta = $this->getPostStatusAndMetaData($post_id);
        $terms = $this->getTermsByPostId($post_id);
        $data = array_merge(['id' => $post_id, 'post' => $post], $statusAndMeta, $terms);
        $message = [
            'properties' => new ArrayObject(),
            'routing_key' => 'routing_posts',
            "payload" => $data,
            'payload_encoding' => 'json'
        ];

        $this->restClient->addHeaders(['accept' => 'application/vnd.ca-consumer.v1+json']);
        $this->restClient->post($message);
    }

    /**
     * @param $post_id
     * @throws HttpException
     */
    public function add_delete_notification($post_id)
    {
        // If this is just a revision, don't send
        if (wp_is_post_revision($post_id)) {
            return;
        }

        $status = get_post_status($post_id);
        $data = ['id' => $post_id, 'status' => $status];
        $message = [
            'properties' => new ArrayObject(),
            'routing_key' => 'routing_posts',
            "payload" => $data,
            'payload_encoding' => 'json'
        ];
        $this->restClient->addHeaders(['accept' => 'application/vnd.ca-consumer.v1+json']);
        $this->restClient->post($message);
    }

    /**
     * @param int $postId
     * @return array
     */
    protected function getPostStatusAndMetaData($postId) {
        $postId = (int) $postId;
        $status = get_post_status($postId);
        $meta = get_post_meta($postId);
        $postMeta = new Post_Meta_Unserializer($meta);

        return ['status' => $status, 'meta' => $postMeta->get()];
    }

    /**
     * @param $post
     * @param string $taxonomy
     * @return array
     */
    protected function getTermsByPostId($post, $taxonomy='category') {
        $terms = get_the_terms($post, $taxonomy);
        return ['categories' => $terms];

    }

    /**
     * @param $term_id
     * @param $tt_id
     * @param $taxonomy
     * @throws HttpException
     */
    public function add_created_term($term_id, $tt_id, $taxonomy) {
        $payload['id'] = $term_id;
        $payload['taxonomy'] = get_term($term_id, $taxonomy);

        $message = $this->createMessage(
            new ArrayObject(),
            'routing_taxonomies',
            $payload
        );

        $this->restClient->addHeaders(['accept' => 'application/vnd.ca-consumer.v1+json']);
        $this->restClient->post($message);
    }

    /**
     * @param $term_id
     * @param $tt_id
     * @param $taxonomy
     * @throws HttpException
     */
    public function add_edited_term($term_id, $tt_id, $taxonomy) {
        $payload['id'] = $term_id;
        $payload['taxonomy'] = get_term($term_id, $taxonomy);

        $message = $this->createMessage(
            new ArrayObject(),
            'routing_taxonomies',
            $payload
        );

        $this->restClient->addHeaders(['accept' => 'application/vnd.ca-consumer.v1+json']);
        $this->restClient->post($message);
    }

    /**
     * @param $term_id
     * @param $tt_id
     * @param $taxonomy
     * @throws HttpException
     */
    public function add_delete_term($term_id, $tt_id, $taxonomy, $deleted_term, $object_ids) {
        $payload['id'] = $term_id;

        $message = $this->createMessage(
            new ArrayObject(),
            'routing_taxonomies',
            $payload
        );

        $this->restClient->addHeaders(['accept' => 'application/vnd.ca-consumer.v1+json']);
        $this->restClient->delete($message);

    }
    public function add_added_option($option, $value) {
        $payload = $this->getAllOptions();

        $message = $this->createMessage(
            new ArrayObject(),
            'routing_options',
            $payload
        );

        $this->restClient->addHeaders(['accept' => 'application/vnd.ca-consumer.v1+json']);
        $this->restClient->post($message);
    }
    public function add_deleted_option($option) {
        $payload = $this->getAllOptions();

        $message = $this->createMessage(
            new ArrayObject(),
            'routing_options',
            $payload
        );

        $this->restClient->addHeaders(['accept' => 'application/vnd.ca-consumer.v1+json']);
        $this->restClient->post($message);
    }
    /*
    public function add_updated_option($option, $old_value, $value) {
        $payload = $this->getAllOptions();
        unset($payload['rewrite_rules']);
        $meta = new Post_Meta_Unserializer($payload);

        $message = $this->createMessage(
            new ArrayObject(),
            'routing_options',
            $meta->get()
        );
        //var_dump(count($payload->get()));
        //var_dump(json_encode($meta->get())); exit;
        $this->restClient->addHeaders(['accept' => 'application/vnd.ca-consumer.v1+json']);
        $this->restClient->post($message);
    }
    */
    /**
     * @return array
     */
    private function getAllOptions() {
        return wp_load_alloptions();
    }

    /**
     * @param ArrayObject $properties
     * @param string $routingKey
     * @param array $payload
     * @param string $payloadEncoding
     *
     * @return array
     */
    public function createMessage($properties, $routingKey, $payload, $payloadEncoding = 'json') {
        return [
            'properties' => $properties,
            'routing_key' => $routingKey,
            "payload" => $payload,
            'payload_encoding' => $payloadEncoding
        ];
    }
}

<?php

/**
 * Class Tortuga_Content_Notifier_Admin_Options
 */
class Tortuga_Content_Notifier_Admin_Options {

	public function __construct() {}

    /**
     * @param $identifier
     * @return mixed|void
     */
    public function getOptions($identifier) {
        return get_option($identifier);
    }

    /**
     * @param $endpoint
     * @return string
     */
    public static function get_endpoint($endpoint) {
        // get tcn-admin-options form values
        $options = self::getOptions('tcn_admin_options');

        // build endpoint url
        $url = $options[$endpoint . '_host']
            . ':'
            . $options[$endpoint .'_port']
            . '/'
            . $options[$endpoint .'_endpoint'];

        return $url;
    }
}

<?php

class Notifier
{
    /**
     * @var Rest_Client
     */
    private $client;
    /**
     * @param Rest_Client $client
     */
    public function __construct(Rest_Client $client) {
        $this->client = $client;
    }
    //{"properties":{},"routing_key":"my key","payload":"my body","payload_encoding":"string"}
    /**
     * @param int $post_id
     * @param object $post
     * @param boolean $isUpdated
     * @throws HttpException
     */
    public function add($post_id, $post, $isUpdated) {
        $message = ['properties' => new ArrayObject(), 'routing_key' => 'my_key', "payload" => '', 'payload_encoding' => 'string'];
        $data = ['post_id' => $post_id, 'post' => $post, 'is_updated' => $isUpdated];
        $this->client->post($data);
    }

    public function send() {
        add_action('save_post', array($this, 'add'), 10, 3);
    }
}
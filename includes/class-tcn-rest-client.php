<?php
use GuzzleHttp\Client;

/**
 * Class Rest_Client
 */
class Rest_Client
{
    const HTTP_POST = 'POST';
    const HTTP_DELETE = 'DELETE';
    /**
     * @var String $url
     */
    private $url;
    /**
     * @var \GuzzleHttp\Client
     */
    private $client;
    /**
     * @var array
     */
    private $httpCodes = [200, 201, 204];
    /**
     * @var array
     */
    private $headers = ['headers' => ['accept' => 'application/json', 'content-type' => 'application/json']];

    public function __construct($url, $options = array())
    {
        $this->url = $url;
        $this->options = $options;
        $this->client = new Client(
            $options
        );
    }

    /**
     * @param $data
     * @throws HttpException
     */
    public function post(array $data)
    {
        $options = [
            'headers' => $this->headers['headers'],
            'json' => $data
        ];
        $response = $this->client->request(
            self::HTTP_POST,
            $this->url,
            $options
        );



        if (!in_array($response->getStatusCode(), $this->httpCodes)) {
            throw new \HttpException(
                sprintf(
                    "Wrong Http-Code: %d .",
                    $response->code
                ),
                $response->getStatusCode()
            );
        }
    }

    /**
     * @param $data
     * @throws HttpException
     */
    public function delete(array $data)
    {
        $options = [
            'headers' => $this->headers['headers'],
            'json' => $data
        ];
        $response = $this->client->request(
            self::HTTP_DELETE,
            $this->url,
            $options
        );



        if (!in_array($response->getStatusCode(), $this->httpCodes)) {
            throw new \HttpException(
                sprintf(
                    "Wrong Http-Code: %d .",
                    $response->code
                ),
                $response->getStatusCode()
            );
        }
    }

    public function addHeaders(array $headers) {
        $this->headers['headers'] = array_merge($this->headers['headers'], $headers);
    }
}
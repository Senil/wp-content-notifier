<?php
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

/**
 * Class AMQP_Publisher
 */
class Publisher
{
    /**
     * @var array
     */
    private $options = [];
    /**
     * @var \PhpAmqpLib\Channel\AMQPChannel
     */
    private $channel;
    /**
     * @var AMQPStreamConnection
     */
    private $connection;
    /**
     * @var PhpAmqpLib\Message\AMQPMessage
     */
    private $message;

    private $exchangeName;

    /**
     * @param StreamConnection $connection
     * @param AMQPMessage $message
     */
    public function __construct(StreamConnection $connection, AMQPMessage $message) {

        $this->message    = $message;
        $this->connection = $connection;
        $this->channel    = $this->connection->channel();
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @return \PhpAmqpLib\Channel\AMQPChannel
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * @return AMQPStreamConnection
     */
    public function getConnection()
    {
        return $this->connection;
    }
    /*
        name: $queue
        passive: false
        durable: true // the queue will survive server restarts
        exclusive: false // the queue can be accessed in other channels
        auto_delete: false //the queue won't be deleted once the channel is closed.
    */
    /**
     * @example
     * name: $queue
     * passive: false
     * durable: true // the queue will survive server restarts
     * exclusive: false // the queue can be accessed in other channels
     * auto_delete: false //the queue won't be deleted once the channel is closed.
     *
     * @param $name
     * @param $options
     * @return $this
     */
    public function declareQueue($name, $options) {
        $this->channel->queue_declare(
            $name,
            $options['passive'],
            $options['durable'],
            $options['exclusive'],
            $options['auto_delete']
        );

        return $this;
    }

    /**
     * @param $name
     * @param $options
     * @return $this
     */
    public function declareExchange($name, $options) {
        $this->channel->exchange_declare(
            $name,
            $options['type'],
            $options['passiv'],
            $options['durable'],
            $options['auto_delete']
        );

        $this->exchangeName = $name;

        return $this;
    }

    /**
     *
     */
    public function basic_publish() {
        $this->channel->basic_publish($this->message, $this->exchangeName);

        $this->channel->close();
        $this->connection->close();
    }
}
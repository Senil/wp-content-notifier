<?php
use PhpAmqpLib\Connection\AMQPStreamConnection;

/**
 * Class AMQP_Publisher
 */
class Stream_Connection
{
    /**
     * @var array
     */
    private $options = [];
    /**
     * @var AMQPStreamConnection
     */
    private $connection;

    /**
     * @param array $options
     */
    public function __construct(array $options) {

        $this->options = $options;

        $this->connection =  new AMQPStreamConnection(
            $this->options['host'],
            $this->options['port'],
            $this->options['user'],
            $this->options['password'],
            $this->options['vhost']
        );
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @return AMQPStreamConnection
     */
    public function getConnection()
    {
        return $this->connection;
    }
}
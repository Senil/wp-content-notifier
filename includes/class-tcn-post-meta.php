<?php

class Post_Meta_Unserializer
{
    /**
     * @var array
     */
    private $data;

    /**
     * @param $data
     */
    public function __construct($data) {
        $this->unserialize($data);

    }

    /**
     * @todo make it recursive
     * @return array|mixed
     */
    public function unserialize(&$data) {
        if (is_array($data)) {
            // check each value
            foreach($data as $index => $entry) {
                if(is_array($entry)) {
                    $this->unserialize($entry);
                } else {
                    if($this->isSerialized($entry)) {
                        $this->data[$index] = unserialize($entry);
                    }
                }
            }
        }
    }

    /**
     * @return array
     */
    public function get()
    {
        return $this->data;
    }

    /**
     * @param $data
     * @return bool
     */
    public function isSerialized($data) {
        return is_serialized($data);
    }
}